import React, { Component } from "react";
import { connect } from "react-redux";
import action from "../../redux/actions/action";

class AddCourse extends Component {
  state = {
    title: "",
    author: "",
    category: "",
    length: ""
  };

  //Handle on change
  handleOnChange = event => {
    const e = event;
    this.setState({ [e.target.name]: e.target.value });
  };

  //On submit course form
  submitForm = e => {
    e.preventDefault();
    let data = {
      id: new Date(),
      title: this.state.title,
      author: this.state.author,
      category: this.state.category,
      length: this.state.length
    };
    this.props.addCourse(data);
    this.props.history.push("/");
  };

  render() {
    return (
      <div className="container">
        <h2>Add Course</h2>
        <div className="addCourseForm">
          <form action="/">
            <div className="form-group">
              <label>Title</label>
              <input
                required
                type="text"
                onChange={this.handleOnChange}
                className="form-control"
                name="title"
                value={this.state.title}
              />
            </div>
            <div className="form-group">
              <label>Author:</label>
              <input
                type="text"
                className="form-control"
                onChange={this.handleOnChange}
                name="author"
                value={this.state.author}
              />
            </div>
            <div className="form-group">
              <label>Category</label>
              <input
                type="text"
                className="form-control"
                onChange={this.handleOnChange}
                name="category"
                value={this.state.category}
              />
            </div>
            <div className="form-group">
              <label>Length</label>
              <input
                type="text"
                className="form-control"
                onChange={this.handleOnChange}
                name="length"
                value={this.state.length}
              />
            </div>
            <input type="button" value="Submit" onClick={this.submitForm} />
          </form>
        </div>
      </div>
    );
  }
}

const mapDispatchToProps = dispatch => {
  return {
    // dispatching plain actions
    addCourse: data => dispatch({ type: action.ADD_COURSE, data: data })
  };
};

export default connect(null, mapDispatchToProps)(AddCourse);
