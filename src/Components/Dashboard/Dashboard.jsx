import React, { Component } from "react";
import { Route, BrowserRouter as Router } from "react-router-dom";
import AddCourse from "../AddCourse";
import EditCourse from "../UpdateCourse";
import View from "../View";

class Dashboard extends Component {
  render() {
    return (
      <div>
        <Router>
          <div>
            <Route path="/" exact component={View} />
            <Route path="/add" component={AddCourse} />
            <Route path="/edit/:id" component={EditCourse} />
          </div>
        </Router>
      </div>
    );
  }
}

export default Dashboard;
