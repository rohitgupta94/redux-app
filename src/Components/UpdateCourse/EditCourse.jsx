import React, { Component } from "react";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import action from "../../redux/actions/action";

class EditCourse extends Component {
  state = {
    title: "",
    author: "",
    category: "",
    length: ""
  };

  //Handle on change
  handleOnChange = event => {
    const e = event;
    this.setState({ [e.target.name]: e.target.value });
  };

  submitForm = e => {
    e.preventDefault();
    let data = {
      id: new Date(),
      title: this.state.title,
      author: this.state.author,
      category: this.state.category,
      length: this.state.length
    };
    this.props.updateCourse(data, this.props.match.params.id);
    this.props.history.push("/");
  };

  render() {
    let formData = this.props.data.filter(course => {
      if (course.id == this.props.match.params.id) {
        return course;
      }
    });
    return (
      <div className="container">
        <h2>Edit Course</h2>
        <div className="addCourseForm">
          <form action="/">
            <div className="form-group">
              <label>Title</label>
              <input
                required
                type="text"
                className="form-control"
                name="title"
                defaultValue={formData[0] ? formData[0].title : ""}
                onChange={this.handleOnChange}
              />
            </div>
            <div className="form-group">
              <label>Author:</label>
              <input
                type="text"
                className="form-control"
                name="author"
                defaultValue={formData[0] ? formData[0].author : ""}
                onChange={this.handleOnChange}
              />
            </div>
            <div className="form-group">
              <label>Category</label>
              <input
                type="text"
                className="form-control"
                name="category"
                defaultValue={formData[0] ? formData[0].category : ""}
                onChange={this.handleOnChange}
              />
            </div>
            <div className="form-group">
              <label>Length</label>
              <input
                type="text"
                className="form-control"
                name="length"
                defaultValue={formData[0] ? formData[0].length : ""}
                onChange={this.handleOnChange}
              />
            </div>
            <input type="button" value="Submit" onClick={this.submitForm} />
            <Link className="btn btn-default" to="/">
              Cancel
            </Link>
            &nbsp;
          </form>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    data: state
  };
};

const mapDispatchToProps = dispatch => {
  return {
    // dispatching plain actions
    updateCourse: (data, courseId) =>
      dispatch({ type: action.UPDATE_COURSE, data: data, id: courseId })
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(EditCourse);
