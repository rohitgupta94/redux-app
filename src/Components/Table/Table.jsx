import React, { Component } from "react";
import { connect } from "react-redux";
import action from "../../redux/actions/action";

class Table extends Component {
  // constructor(props) {
  //     super(props)
  // }

  onClickHandler = e => {
    if (e.target.closest("tr").classList.contains("activeClass")) {
      e.target.closest("tr").classList.remove("activeClass");
      this.props.getActiveElementId(null);
    } else {
      e.target.closest("tr").classList.add("activeClass");
      this.props.getActiveElementId(e.target.closest("tr").getAttribute("id"));
    }
  };

  createTableBody = result => {
    if (result) {
      let rows = result.map((row, index) => {
        return (
          <tr key={index} id={row.id} onClick={this.onClickHandler}>
            <td>{row.title}</td>
            <td>{row.length}</td>
            <td>{row.category}</td>
            <td>{row.author}</td>
            <td>
              <button
                onClick={() =>
                  this.props.dispatch({
                    type: action.DELETE_COURSE,
                    id: row.id
                  })
                }
              >
                Delete
              </button>
            </td>
          </tr>
        );
      });
      return rows;
    }
    return "";
  };

  render() {
    return (
      <table>
        <thead>
          <tr>
            <th>Title</th>
            <th>Length</th>
            <th>Category</th>
            <th>Author</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody>{this.createTableBody(this.props.data)}</tbody>
      </table>
    );
  }
}
const mapStateToProps = state => {
  return {
    data: state
  };
};
export default connect(mapStateToProps)(Table);
