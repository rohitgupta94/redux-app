import React, { Component } from "react";
import Table from "../Table";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import "./view.css";

class View extends Component {
  state = {
    activeElementId: null
  };

  //Get the active element id and set state
  getActiveElementId = id => {
    this.setState({ activeElementId: id });
  };

  render() {
    return (
      <div className="container listing-page">
        <h2>Courses</h2>
        <div className="top-header">
          <Link className="btn btn-success form_button_style" to="/add">
            Add
          </Link>
          &nbsp;
          <Link
            className="btn btn-warning form_button_style"
            to={
              this.state.activeElementId
                ? "/edit/" + this.state.activeElementId
                : ""
            }
          >
            Edit
          </Link>
          &nbsp;
        </div>
        <div className="form">
          <h4>Listing</h4>
          <hr />
          <Table getActiveElementId={this.getActiveElementId} />
        </div>
      </div>
    );
  }
}

export default connect()(View);
