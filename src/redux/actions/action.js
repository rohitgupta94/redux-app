const Types = {
    ADD_COURSE: "ADD_COURSE",
    DELETE_COURSE: "DELETE_COURSE",
    UPDATE_COURSE: "UPDATE_COURSE"
};

export default Types;