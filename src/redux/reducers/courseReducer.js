const courseReducer = (state = [], action) => {
  debugger
  switch (action.type) {
    case 'ADD_COURSE':
      return state.concat([action.data]);
    case 'DELETE_COURSE':
      return state.filter((course) => course.id !== action.id);
    case 'UPDATE_COURSE':
      return state.map((course) => {
        if (course.id == action.id) {
          return {
            ...course,
            title: action.data.title,
            author: action.data.author,
            category: action.data.category,
            length: action.data.length
          }
        } else return course;
      });
    default:
      return state;
  }
}
export default courseReducer;